import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * A Breadth-First-Search function designed to find shortest path between two characters, in this case designed to be runnable as a thread
 * for multi-breadth-first-search.
 * 
 * @param String character1, String character2, CharacterGraph mygraph,  HashMap<Integer, LinkedList<String>> pathlist 
 * @return nothing
 * @effects Changes the pathlist by adding in a found pathway from character1 to character2. Each thread may find a different path as such will add different entries into pathlist.
 */

public class shortestpath implements Runnable{
	
	
	// Our shortestpath thread object will hold a few things
	// characterA/B, basically start and finish characters.
	// edgeGraph is our pointer to our graph with characters and edges.
	// queue, pathmap, pathlist are all the components of a single breadth first search. 
	String characterA;				
	String characterB;
	CharacterGraph edgeGraph;
	LinkedList<String> queue = new LinkedList<String>();
	Map<String, LinkedList<String>> Pathmap = new HashMap<String, LinkedList<String>>();
	HashMap<Integer, LinkedList<String>> pathlist = new HashMap<Integer, LinkedList<String>>();
	public void run(){
		
		// Here is our lock initialization for later when we need to ensure thread safety for the pathlist.
		Object lock = new Object();
		
		// This part and below is your standard BFS shell, adapted from the github MP5 site.
		queue.addFirst(characterA);
		LinkedList<String> empty = new LinkedList<String>();
		Pathmap.put(characterA, empty);
		int searchlevel = 0;
		
		while (queue.isEmpty() == false) {
			
			String next = queue.removeFirst();
			if ( next.equals(characterB)){
				
				
				// Here is the situation where vertex2 is found, and so we submit our path into the pathlist. We need to synchronize lock the pathlist so we don't have multiple threads
				// simultaneously interacting with the pathlist at once.
				synchronized (lock) {
					
					if ( pathlist.keySet().contains(searchlevel) == false){
						pathlist.put(searchlevel, Pathmap.get(next));
						return;
					}
					
				}
				
			}
			// Here is the part where we need to traverse deeper into the tree.
			// We first identify all of the neighbors of the current node.
			// Then we organize this neighbor list lexically according to expectations before adding each neighbor into the queue for further searching.
			ArrayList<String> neighbor = new ArrayList<String>();
			neighbor.addAll(edgeGraph.Vertices.get(next).keySet());
			
			Collections.sort(neighbor, new Comparator<String>(){
				
				@Override
				public int compare(String String1, String String2){
					return String1.compareToIgnoreCase(String2);
				}
			});
			
			int tempsize = neighbor.size();
			for (int i = 0; i < tempsize; i++){
				if (Pathmap.containsKey(neighbor.get(i)) == false){
					LinkedList<String> templist = new LinkedList<String>();
					templist.addAll(Pathmap.get(next));
					templist.add(next);
					Pathmap.put(neighbor.get(i), templist);
					queue.addFirst(neighbor.get(i));
				}
			}
			searchlevel++;
		}
	} 


	public shortestpath (String character1, String character2, CharacterGraph mygraph, HashMap<Integer, LinkedList<String>> pathlist ) {
		characterA = character1;
		characterB = character2;
		edgeGraph = mygraph;	
		this.pathlist = pathlist;
	}
	
	
}