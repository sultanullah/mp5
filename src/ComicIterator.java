import java.io.*;

/**
 * This class helps to iterate over a file with the information
 * for a character and then comic he/she/it belonged in
 * 
 * @author Sultan Ullah and Kevin Chan
 * 
 */
public class ComicIterator {

	private boolean next;
	private BufferedReader inputReader;
	private String nextRow;

	/**
	 * Create a ComicIterator given a filename.
	 * 
	 * @param fileName
	 *            the name of the file to open.
	 * @throws IOException
	 *             if there was a problem reading the file.
	 */
	public ComicIterator(String fileName) throws IOException {

		// open the file
		inputReader = new BufferedReader(new InputStreamReader(
				new FileInputStream(fileName)));

		// read the first line and set the state of the iterator
		if ((nextRow = inputReader.readLine()) != null) {
			next = true;
		} else {
			next = false;
		}
	}

	/**
	 * Check if there is more data that the iterator can process.
	 * 
	 * @return true if there is more data to process otherwise return false.
	 */
	public boolean hasNext() {
		return next;
	}

	/**
	 * Return the next character and comic from the iterator.
	 * 
	 * @return the next character in the list. Requires that hasNext() is true
	 *         otherwise null is returned.
	 */
	public Character getNext() throws IOException {

		if (!next) {
			// we are the end of the stream so return null.
			// ideally the client should have checked hasNext() and this should
			// not be necessary
			return null;
		} else {

			// we should first replace the | character with tab spacing to
			// simplify regex matching
			nextRow = nextRow.replaceAll("\"", "");

			// now split the string at the tabs to create columns
			String[] columns = nextRow.split("\t");

			// the zeroth column represents the movie id number
			String name = columns[0];
			String comic = columns[1];


			// advance the iterator state wrt the input stream
			if ((nextRow = inputReader.readLine()) != null) {
				next = true;
			} else {
				next = false;
			}
			

			return new Character(name, comic);
		}
	}
}