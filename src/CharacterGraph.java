import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

//This class that represents an undirected graph with comic book characters as vertices.
//The edges represent the fact that two characters appeared in the same comic book.
//This graph should be immutable except for the addition of vertices and edges. 
//It should not be possible to change a vertex after it has been added to the graph.

/*abstraction function: AF : R → A
* the space of representation values in the CharacterGraph class is character objects 
* created using the Character class which are each mapped to the abstract concept of a vertex on a graph. 
* The abstract graph is represented by a map which holds all the vertices currently on the graph object. 
*/

/*representation invariant: RI : R → boolean
* When performing operations on Characters in the CharacterGraph class, they must be a non-null vertices 
* on the abstract graph. Edges between two Characters must be strings that represent the fact they appeared 
* in a comic book together
*/

public class CharacterGraph {
	
	//represents the collection of vertices on the graph
	public Map<String, Map<String, List<String>>> Vertices = new HashMap<String, Map<String, List<String>>>();
	
	/**
	 * Add a new character to the graph. If the character already exists in the graph
	 * then this method will return false. Otherwise this method will add the
	 * character to the graph and return true.
	 * 
	 * @param character
	 *            the character to add to the graph. Requires that character != null.
	 * @return true if the character was successfully added and false otherwise.
	 * @modifies this by adding the character to the graph if the character did not
	 *           exist in the graph.
	 */	
	public boolean addVertex(Character character) {
		//check to see if character exists on the graph already
		ArrayList<String> tempList = new ArrayList<String>();
		tempList.addAll(Vertices.keySet());
		for(int i = 0; i < tempList.size(); i++){
			if(tempList.get(i).equals(character.getName())){				
				return false;
			}
		}
		//add the character to the graph and return true
		Vertices.put(character.getName(), new HashMap<String, List<String>>());
		return true;
	}
	
	/**
	 * Add a new edge to the graph. If the edge already exists in the graph then
	 * this method will return false. Otherwise this method will add the edge to
	 * the graph and return true.
	 * 
	 * @param movie1
	 *            one end of the edge being added. Requires that character1 != null.
	 * @param movie2
	 *            the other end of the edge being added. Requires that character2 !=
	 *            null. Also require that character1 is not equal to character2 because
	 *            self-loops are not meaningful in this graph.
	 * @param comic
	 *            represents the comic book that both characters appeared in
	 * @return true if the edge was successfully added and false otherwise.
	 * @modifies this by adding the edge to the graph if the edge did not exist
	 *           in the graph.
	 */
	public boolean addEdge(String character1, String character2, String comic){
		//check to see if edge exists already
		if((Vertices.get(character1).containsKey(character2))){
			if(Vertices.get(character1).get(character2).contains(comic)){
				return false;
				} else {
					List<String> currentList = Vertices.get(character1).get(character2);
					currentList.add(comic);
					Vertices.get(character1).put(character2, currentList);
					Vertices.get(character2).put(character1, currentList);
					return true;
				}
			} else {
				//add edge to graph
				List<String> emptyList = new ArrayList<String>();
				emptyList.add(comic);
				Vertices.get(character1).put(character2, emptyList);
				Vertices.get(character2).put(character1, emptyList);
				return true;		
				
				}
			}
		
	/**
	 * Return the shortest path between two characters in the graph.
	 * 
	 * @param character1
	 *            the character at one end of the path.
	 * @param character2
	 *            the character at the other end of the path.
	 * @return the shortest path, as a list, between the two characters represented
	 *         by their names. This path begins at the character represented by
	 *         character1 and ends with the character represented by character2.
	 */
	public List<String> getShortestPath(Character character1, Character character2) {
	
		//this is the breadth-first search algorithm
		String start = character1.getName();
		String dest = character2.getName();
		Queue<String> Q = new LinkedList<String>();
		Map<String, ArrayList<String>> M = new HashMap<String, ArrayList<String>>();
		
		//a path is a list of nodes(may need to contain information about the comic)
		
		Q.add(start);
		M.put(start, new ArrayList<String>());
		
		while(!Q.isEmpty()){
			String n = Q.poll();
			if(n.equals(dest)){
				return M.get(n);
			}
			ArrayList<String> neighbourList = new ArrayList<String>();
			neighbourList.addAll(Vertices.get(n).keySet());
			
			
			//to make sure it is in lexographic order
			Collections.sort(neighbourList, new Comparator<String>(){
			
			@Override
			public int compare(String String1, String String2){
				return String1.compareToIgnoreCase(String2);
			}
		});
			
			for(int m = 0; m < neighbourList.size(); m++){
				if(!M.containsKey(neighbourList.get(m))){
					Q.add(neighbourList.get(m));
					ArrayList<String> L = new ArrayList<String>();
					L.addAll(M.get(n));
					L.add(n + " and " + neighbourList.get(m) + " appear in " + Vertices.get(n).get(neighbourList.get(m)).get(0));
					M.put(neighbourList.get(m),L);
				}
			}
		}
		//return empty list if not found
		return new ArrayList<String>();
	}
}