import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

// Kevin Chan 39975131 Sultan Ullah 29128113

/**
 * A function that will find the shortest path between two characters in a given database.
 * 
 * @param
 * 		String			character1,character2		The two characters you want the shortest path for.
 * 		Integer			threadmax					the number of threads you wish to initialize for this search
 * 		String			filename					The filename of the database you are searching from.
 * 
 * @return 		Nothing
 * @effects		Will display the shortest path found in the console.
 * 
 */
public class MP5 {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws IOException, InterruptedException {
	
	
	// Put the commandline stuff here
	String filename = args[0];
	String vertex1 = args[1];
	String vertex2 = args[2];
	int threadmax = Integer.parseInt(args[3]);
		
		
		

	// First create some graphs.
	System.out.print("Creating graphs..." + "\n");
	CharacterGraph mygraph = new CharacterGraph();										// the main graph, a hashmap with keys = characters, values = edges.
	Map<String, LinkedList<String>> Maps = new HashMap<String, LinkedList<String>>();	// A separate map containing just a list of all characters.
	LinkedList<String> Maps2 = new LinkedList<String>();
	

	
	// Comic iterator
	// This will go through the data file of all the comic heroes and place them into our graph.
	System.out.print("Adding characters..." + "\n");
	try {																// Make sure we have a correct/valid file.
	ComicIterator iter = new ComicIterator(filename);	
	} catch (IOException e){
		System.out.println("Bad file!");
	}
	ComicIterator iter = new ComicIterator(filename);	
	while ( iter.hasNext() ) {
			

		Character char1 = iter.getNext();
		mygraph.addVertex(char1);										// Add the character to our edge map
		
		
		// The following will add the character to our "Maps" graph, which is a map of each character with a list of their comic book appearances.
		if (Maps.containsKey(char1.getName()))
			Maps.get(char1.getName()).addAll(char1.getComic());
		else {
			Maps.put(char1.getName(), char1.getComic());
			Maps2.add(char1.getName());
		}		
		
	}
	

	
	// Iterate through our newly filled graph to fill edges.
	System.out.print("Adding Edges..." + "\n");
	
	LinkedList<String> Maps4 = (LinkedList<String>) Maps2.clone();
	for ( String current : Maps2 ) {
		
		LinkedList<String> Maps3 = (LinkedList<String>) Maps4.clone();
		
		while ( Maps3.isEmpty() != true){
			
			if (current != Maps3.peek()){
				
				
				LinkedList<String> comicList = Maps.get(current);
				LinkedList<String> comicList1 = (LinkedList<String>) comicList.clone();
				comicList = Maps.get(Maps3.peek());
				LinkedList<String> comicList2 = (LinkedList<String>) comicList.clone();
				
				if ( comicList1.retainAll(comicList2)){		
					int size = comicList1.size();
					for ( int i = 0; i < size; i++ ){
						//System.out.print(current + " AND " + Maps3.peek() + "\n");
						mygraph.addEdge(current, Maps3.peek(), comicList1.getFirst());

						comicList1.pollFirst();
					}			
				}	
			}
			Maps3.poll();
		}
	
		Maps4.remove(current);
	}
	

			


	// Does the threadmax violate the invariant?
	if ( (threadmax <= mygraph.Vertices.get(vertex1).keySet().size()) == true ){
		
		// If not proceed and start the multi-threading.
		// So create an arraylist to hold all the threads, then fill it according to how many threads set by the threadmax.
		// Thread safety is ensured via an object lock on the pathlist Hashmap being passed onto each thread. Each thread will concurrently run however
		// only one thread at a given moment may access the pathlist Hashmap to enter in found paths.
		ArrayList<Thread> Threadlist = new ArrayList<Thread>();
		HashMap<Integer, LinkedList<String>> pathlist = new HashMap<Integer, LinkedList<String>>();
		ArrayList<String> neighbors = new ArrayList<String>();
		neighbors.addAll(mygraph.Vertices.get(vertex1).keySet());
		
		// Create threads based on threadmax appointed by user prompt.
		for ( int i = 0; i < threadmax; i++){		
			Threadlist.add(new Thread(new shortestpath(neighbors.get(i), vertex2, mygraph, pathlist)));
		}
		
		// Initiate all threads
		for ( int i2 = 0; i2 < Threadlist.size(); i2++) {
			Threadlist.get(i2).run();
		}
		
		// The following waits and checks for all threads to finish contributing if possible to the pathlist.
		boolean done = false;
		
		while ( done != true){
			
			Thread.sleep(250);
			for (Thread temp : Threadlist) {
				
				if ( temp.isAlive()){
					done = false;
					break;
				}
				else {
					done = true;
				}
			}		
		}
		
		// If pathlist has no entries, let the user know.
		if (pathlist.size() == 0)
			System.out.print("No path found!" + "\n");
		
		// If it does have entries, we mapped the entries based on the "levels" of where the path is identified. Higher the level, the more neighbors traversed, so we'll take
		// the path with the lowest level.
		else{
		int result = 0;
		ArrayList<Integer> pathtemp = new ArrayList<Integer>();
		pathtemp.addAll(pathlist.keySet());
		result = pathtemp.get(0);
		for ( int i3 = 0; i3 < pathlist.size(); i3++) {
			if (pathtemp.get(i3) < result)
				result = pathtemp.get(i3);
		}
		
		
		// Now we will do some fancy type-setting and print out the shortest path starting with each relation and their respective comic book appearances.
		ArrayList<String> finalList = new ArrayList<String>();
		finalList.addAll(pathlist.get(result));
		String A = vertex1;
		for (int i4 = 0; i4 < finalList.size(); i4++){
			
			System.out.print(A + " and " + finalList.get(i4) + " appeared in " + mygraph.Vertices.get(A).get(finalList.get(i4)).get(0) + "\n");
			
				A = finalList.get(i4);
				
				if ( i4 == finalList.size()-1)
					System.out.print(A + " and " + vertex2 + " appeared in " + mygraph.Vertices.get(A).get(vertex2).get(0) + "\n");
		}
		
		
		
		}
		
	} else {
		System.out.print("Bad parameters, thread count needs to be less than the amount of neighbors of character one/vertex one.");
	}

	}	

}