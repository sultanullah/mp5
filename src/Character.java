import java.util.LinkedList;
import java.util.List;
/**
 * this class represents a character and the comic they are in
 */

public class Character {

	private String Name;
	private LinkedList<String> Comic = new LinkedList<String>();
	
	/**
	 * Create a new Character object with the given information.
	 * 
	 * @param Name
	 *            the name of the character
	 * @param Comic
	 *            the comic in which this character appeared
	 */
	public Character(String Name, String Comic) {
		this.Name = Name;
		this.Comic.add(Comic);
	}
	
	/**
	 * Return the name of the character
	 * 
	 * @return character name
	 */
	public String getName(){
		return Name;
	}
	
	/**
	 * Return the list of comics this character appeared in
	 * 
	 * @return comic the list of comics for this character
	 */
	public LinkedList<String> getComic(){
		return Comic;
	}
	

	/**
	 * Return a string representation of the character
	 * 
	 * @return String in the format "Character, Comic"
	 */
	public String toString(){
		return Name + "," + Comic;
	}
	
	
	
}